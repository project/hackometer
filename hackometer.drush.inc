<?php

/**
 * @file
 *
 * Drush script for site maintainers quantifying how "broken" the install is.
 *
 * Black points are awarded for:
 * - contrib modules, themes under the drupal roots modules/themes folder
 * - custom modules, themes under the drupal roots modules/themes folder
 * - missing htaccess file
 * - failing coder test
 * - using custom modules that have no description
 * - using php code in nodes
 * - using php code in blocks.
 */

/**
 * Implements hook_drush_command().
 */
function hackometer_drush_command() {
  $items = array();
  $items['hackometer'] = array(
    'description' => 'Evaluate the quality of the site based on pre-defined conditions (e.g. having php code in nodes)',
    'aliases' => array('hom'),
    'callback' => 'drush_hackometer_execute',
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_DATABASE,
  );
  return $items;
}

/**
 * Implements hook_drush_help().
 */
function hackometer_drush_help($section) {
  if ($section == 'drush:hackometer') {
    return dt("Command-line tool to evaluate the quality of a drupal installation based on the following:\n
    - where contributed modules are placed\n
    - where custom modules are placed\n
    - if any of the contributed modules have been altered\n
    - if core has been altered\n
    - if the .htaccess file is missing/altered\n
    - if custom code fails coder tests\n
    - if php code is used in nodes\n
    - if php code is used in blocks");
  }
}

function drush_hackometer_execute() {
  $points = 0;
  // Check the filestructure.
  $points += drush_hackometer_check_structure();
  $points += drush_hackometer_check_alter('contrib');
  $points += drush_hackometer_check_alter('core');
  $points += drush_hackometer_check_htaccess();
  $points += drush_hackometer_check_php('nodes');
  $points += drush_hackometer_check_php('blocks');
  print dt("Collected $points points.\n");
}

/**
 * Check if modules are correctly placed under sites/all/modules
 */
function drush_hackometer_check_structure() {
  // Name of core modules and themes that are supposed to be under modules/ and themes/ respectably.
  $core = array();

  // Check if core projects are all right.
  $result = db_query('SELECT name, type, filename FROM {system}');
  while ($project = db_fetch_object($result)) {
    if (in_array($project->name, $core)) {
      $supposed_path = $project->type . 's/' . $project->name . '/' . $project->name;
      $supposed_path .= ($project->type == 'module') ? '.module' : '.info';
      if ($project->filename != $supposed_path) {
        drush_log(dt("Core project $project->name is out of place!", 'warning'));
      }
    }
  }
}

/**
 * Check if core or contrib modules have been altered.
 * @TODO use hacked if present.
 */
function drush_hackometer_check_alter($type) {

}

/**
 * Check if htaccess file is present and unaltered.
 */
function drush_hackometer_check_htaccess() {

}

/**
 * Check if any of the nodes have the php input filter.
 */
function drush_hackometer_check_php($type) {
  $format = db_result(db_query('SELECT format FROM {filter_formats} WHERE name = "PHP code"'));
  switch ($type) {
    case 'nodes':
    $count = db_result(db_query('SELECT COUNT(*) FROM {node_revisions} WHERE format = %d', $format));
    break;
    case 'blocks':
    $count = db_result(db_query('SELECT COUNT(*) FROM {boxes} WHERE format = %d', $format));
    break;
  }
  if ($count > 0) {
    drush_log(dt("$count $type found with PHP input filter."), 'warning');
  }
  return ($count > 0) ? 1 : 0;
}
